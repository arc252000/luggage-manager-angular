import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  url = 'http://localhost:8080/';

  itemResult: Array<any> = [];

  constructor(private http: HttpClient) { }

  getAirportNames(seachString: string) : Observable<String[]> {
    return this.http.get<String[]>(this.url + 'fetchAirports?airportKeyword=' + seachString)
  }

  getAirlineNames(seachString: string) : Observable<String[]> {
    return this.http.get<String[]>(this.url + 'fetchAirlines?airlineIATACodes=' + seachString)
  }

  submitVideo(selectedToAirport: string, selectedAirline: string, file: File) : Observable<String[]> {

    let formData:FormData = new FormData();
    formData.append('video', file, file.name);

    return this.http.post<String[]>(this.url + 'processItems/?airport=' + selectedToAirport + '&airline=' + selectedAirline , formData);
  }

  setItemResult(reuslt: Array<any>){
    this.itemResult = reuslt;
    for (let entry of this.itemResult) {
      console.log('setItemResult', entry)
    }
  }

  getItemResult(){
    console.log('getItemResult' + this.itemResult);
    for (let entry of this.itemResult) {
      console.log('getItemResult', entry)
    }
    return this.itemResult;
  }

}
