import { Component } from '@angular/core';
import { FlightService } from '../services/flight.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-flightdetails',
  templateUrl: './flightdetails.component.html',
  styleUrls: ['./flightdetails.component.css']
})
export class FlightdetailsComponent {

  airportArr: Array<any> = [];
  airlineArr: Array<any> = [];
  itemResult: Array<any> = [];

  selectedFromAirport: string;
  selectedToAirport: string;
  selectedAirline: string;
  flightDate: string;
  file: File;

  constructor(private flightService: FlightService, private router: Router) {

  }

  onKeyAirport(event: KeyboardEvent) {
    this.flightService.getAirportNames( (<HTMLInputElement>event.target).value).subscribe (
      airportNames => {
          this.airportArr = <String[]>airportNames;
          console.log(this.airportArr);
        }
    )
  }

  onKeyAirline(event: KeyboardEvent) {
    this.flightService.getAirlineNames( (<HTMLInputElement>event.target).value).subscribe (
      result => {
          this.airlineArr = <String[]>result;
          console.log(this.airlineArr);
        }
    )
  }

  onChange(event : any) {
    this.file = event.target.files[0];
  }

  submitForm() {
      alert('values are ' + this.selectedFromAirport + ' ' + this.selectedToAirport + ' ' +
      this.selectedAirline + ' ' + this.flightDate + ' ' + this.file.name);
      
      this.flightService.submitVideo(this.selectedToAirport, this.selectedAirline, this.file).subscribe (
        result => {
            this.itemResult = <String[]>result;
            console.log(this.itemResult);
            this.flightService.setItemResult(this.itemResult);
            this.router.navigate(['/result']);
          }
      )
  }
  
}
