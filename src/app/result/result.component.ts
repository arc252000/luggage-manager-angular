import { Component } from '@angular/core';
import { FlightService } from '../services/flight.service';
import { Item } from '../item'

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent {

  items: Array<Item> = [];
  item: Item;

  constructor(private flightService: FlightService) {
    for (let entry of this.flightService.getItemResult()) {
      this.item = entry;
      console.log('constructor', this.item);
      this.items.push(this.item);
    }
  }

}
